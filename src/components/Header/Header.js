import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import GoogleAuth from '../GoogleAuth/GoogleAuth';

class Header extends Component {
    render() {
        return (
            <div className="ui secondary pointing menu">
                <Link to="/" className="item"> MusikGram</Link>
                <div className="right menu">
                <Link to="/" className="item"> Find Music</Link>
                <GoogleAuth/>
                </div>
                
            </div>
        )
    }
}

export default Header;
