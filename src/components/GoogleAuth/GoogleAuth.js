import React, { Component } from 'react'

 class GoogleAuth extends Component {

    state = {isSignedIn: null};

    componentDidMount(){
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({
                clientId: '342550004795-dh4j3jdfl7iff2arcpan4t5ai2t16mgq.apps.googleusercontent.com',
                scope: 'email'
            }).then(()=>{
                this.auth = window.gapi.auth2.getAuthInstance();
                this.setState({isSignedIn: this.auth.isSignedIn.get()});
                this.auth.isSignedIn.listen(this.onAuthChange);
            });
        });
    }


    onSignIn = () =>{
        this.auth.signIn();
    };

    onSignOut = () =>{
        this.auth.signOut();
    };

    onAuthChange = () =>{
        this.setState({isSignedIn: this.auth.isSignedIn.get()});
    };

    renderAuthButton(){
        if(this.state.isSignedIn === null){
            return null;
        } else if (this.state.isSignedIn){
            return (
            <button onClick={this.onSignOut} className="ui red google button"> <i className="google icon"/>Desconectar</button>
            );
        } else{
            return (
                <button onClick={this.onSignIn} className="ui blue google button"> <i className="google icon"/>Conectar usando Google</button>
                );
        }
    
    }

    render() {
        return (
            <div>
                {this.renderAuthButton()}
            </div>
        )
    }
}

export default GoogleAuth;
